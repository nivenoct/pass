import cards.Season2015CardDefinition;
import cards.not_workday.limited.NotWorkdayLimited100CardDefinition;
import cards.not_workday.limited.NotWorkdayLimited10CardDefinition;
import cards.not_workday.limited.NotWorkdayLimited20CardDefinition;
import cards.not_workday.limited.NotWorkdayLimited50CardDefinition;
import cards.not_workday.unlimited.NotWorkdayUnlimited1DayCardDefinition;
import cards.not_workday.unlimited.NotWorkdayUnlimited2DaysCardDefinition;
import cards.not_workday.unlimited.NotWorkdayUnlimitedFirstHalfOfDayCardDefinition;
import cards.not_workday.unlimited.NotWorkdayUnlimitedSecondHalfOfDayCardDefinition;
import cards.workday.limited.WorkdayLimited100CardDefinition;
import cards.workday.limited.WorkdayLimited10CardDefinition;
import cards.workday.limited.WorkdayLimited20CardDefinition;
import cards.workday.limited.WorkdayLimited50CardDefinition;
import cards.workday.unlimited.*;
import io.ski.CardSystem;
import io.ski.Turnstile;
import io.ski.UserCard;
import io.ski.card.Card;
import io.ski.card.CardDefinition;
import io.ski.repository.CardRepository;
import io.ski.repository.CollectionBasedCardRepository;
import io.ski.repository.generator.AutoIncrementationIdentifierGenerator;
import io.ski.repository.generator.IdentifierGenerator;
import io.ski.statistics.domain.PassStatus;
import io.ski.support.validation.DefaultHolidayResolver;
import io.ski.support.validation.HolidayResolver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {

  public static final int CARD_COUNT = 500;
  public static final int PASS_COUNT = 10000;
  public static final Random RANDOM = new Random();

  public static void main(String[] args) {
    HolidayResolver holidayResolver = new DefaultHolidayResolver();
    IdentifierGenerator identifierGenerator = new AutoIncrementationIdentifierGenerator();
    CardRepository cardRepository = new CollectionBasedCardRepository(identifierGenerator);
    CardSystem cardSystem = new CardSystem(cardRepository, holidayResolver);

    List<CardDefinition<? extends Card>> cardDefinitions = Arrays.asList(
        new WorkdayLimited10CardDefinition(),
        new WorkdayLimited20CardDefinition(),
        new WorkdayLimited50CardDefinition(),
        new WorkdayLimited100CardDefinition(),

        new WorkdayUnlimitedFirstHalfOfDayCardDefinition(),
        new WorkdayUnlimitedSecondHalfOfDayCardDefinition(),
        new WorkdayUnlimited1DayCardDefinition(),
        new WorkdayUnlimited2DaysCardDefinition(),
        new WorkdayUnlimited5DaysCardDefinition(),

        new NotWorkdayLimited10CardDefinition(),
        new NotWorkdayLimited20CardDefinition(),
        new NotWorkdayLimited50CardDefinition(),
        new NotWorkdayLimited100CardDefinition(),

        new NotWorkdayUnlimitedFirstHalfOfDayCardDefinition(),
        new NotWorkdayUnlimitedSecondHalfOfDayCardDefinition(),
        new NotWorkdayUnlimited1DayCardDefinition(),
        new NotWorkdayUnlimited2DaysCardDefinition(),

        new Season2015CardDefinition()
    );

    cardDefinitions.forEach(cardSystem::registerCardType);
    Turnstile turnstile = new Turnstile(cardSystem);

    List<UserCard> cards = new ArrayList<>(CARD_COUNT);
    for (int i = 0; i < CARD_COUNT; i++) {
      int position = RANDOM.nextInt(cardDefinitions.size());
      cards.add(cardSystem.create(cardDefinitions.get(position).getDiscriminator()));
    }

    for (int i = 0; i < PASS_COUNT; i++) {
      int position = RANDOM.nextInt(cards.size());
      turnstile.pass(cards.get(position));
    }

    System.out.printf("%d events overall\n", cardSystem.createEventView().count());
    System.out.printf("%d events related to validation rejection\n", cardSystem.createEventView().filterByStatus(PassStatus.UNAUTHORIZED).count());
    System.out.printf("%d events related to successful passing\n", cardSystem.createEventView().filterByStatus(PassStatus.AUTHORIZED).count());


  }
}
