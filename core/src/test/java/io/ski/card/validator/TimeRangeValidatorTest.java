package io.ski.card.validator;

import io.ski.card.AbstractMockitoTest;
import io.ski.card.type.UnlimitedCard;
import io.ski.support.validation.BindingResult;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.time.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class TimeRangeValidatorTest extends AbstractMockitoTest {

  private static final ZoneId DEFAULT_ZONE_ID = ZoneId.of("UTC");
  private static final ZoneOffset ZERO_ZONE_OFFSET = ZoneOffset.UTC;

  private static final LocalDateTime CARD_VALIDITY_MIDDLE_POINT = LocalDateTime.now();
  public static final LocalDateTime CARD_VALIDITY_START_POINT = CARD_VALIDITY_MIDDLE_POINT.minusHours(1);
  public static final LocalDateTime CARD_VALIDITY_END_POINT = CARD_VALIDITY_MIDDLE_POINT.plusHours(1);

  public static final LocalDateTime TIME_BEFORE_VALIDITY_START_POINT = CARD_VALIDITY_START_POINT.minusHours(1);
  public static final LocalDateTime TIME_AFTER_VALIDITY_END_POINT = CARD_VALIDITY_END_POINT.plusHours(1);

  @Mock protected Clock clock;
  @Mock protected UnlimitedCard card;

  protected BindingResult bindingResult;
  protected TimeRangeValidator<UnlimitedCard> validator;


  @Before
  public void setup() {
    bindingResult = new BindingResult();
    validator = new TimeRangeValidator<>(clock);

    when(clock.getZone()).thenReturn(DEFAULT_ZONE_ID);

    when(card.getStartPoint()).thenReturn(CARD_VALIDITY_START_POINT);
    when(card.getEndPoint()).thenReturn(CARD_VALIDITY_END_POINT);
  }

  @Test
  public void shouldConsiderBeforeValidityStartPointTimeAsError() {
    Instant instant = dateToInstant(TIME_BEFORE_VALIDITY_START_POINT);
    when(clock.instant()).thenReturn(instant);

    validator.validate(card, bindingResult);

    assertTrue(bindingResult.hasErrors());
  }

  @Test
  public void shouldConsiderValidityStartPointTimeAsGood() {
    Instant instant = dateToInstant(CARD_VALIDITY_START_POINT);
    when(clock.instant()).thenReturn(instant);

    validator.validate(card, bindingResult);

    assertFalse(bindingResult.hasErrors());
  }

  @Test
  public void shouldConsiderValidityMiddlePointTimeAsGood() {
    Instant instant = dateToInstant(CARD_VALIDITY_MIDDLE_POINT);
    when(clock.instant()).thenReturn(instant);

    validator.validate(card, bindingResult);

    assertFalse(bindingResult.hasErrors());
  }

  @Test
  public void shouldConsiderValidityEndPointTimeAsGood() {
    Instant instant = dateToInstant(CARD_VALIDITY_END_POINT);
    when(clock.instant()).thenReturn(instant);

    validator.validate(card, bindingResult);

    assertFalse(bindingResult.hasErrors());
  }

  @Test
  public void shouldConsiderAfterValidityEndPointTimeAsError() {
    Instant instant = dateToInstant(TIME_AFTER_VALIDITY_END_POINT);
    when(clock.instant()).thenReturn(instant);

    validator.validate(card, bindingResult);

    assertTrue(bindingResult.hasErrors());
  }


  protected static Instant dateToInstant(LocalDateTime time) {
    return time.toInstant(ZERO_ZONE_OFFSET);
  }

}