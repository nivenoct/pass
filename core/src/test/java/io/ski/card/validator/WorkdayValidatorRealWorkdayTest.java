package io.ski.card.validator;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(Parameterized.class)
public class WorkdayValidatorRealWorkdayTest extends WorkdayValidatorTest {

  private final LocalDate realWorkday;

  public WorkdayValidatorRealWorkdayTest(LocalDate realWorkday) {
    this.realWorkday = realWorkday;
  }

  @Before
  public void setup(){
    initMocks(this);
    super.setup();
  }

  public void shouldConsiderRealWorkdayAsWorkday() {
    when(clock.instant()).thenReturn(dateToInstant(realWorkday));

    validator.validate(card, bindingResult);

    assertFalse(bindingResult.hasErrors());
  }

  @Parameterized.Parameters
  public static Collection<Object[]> realWorkdays() {
    return Arrays.asList(new Object[][]{
        { LocalDate.of(2015, Month.OCTOBER, 5) },
        { LocalDate.of(2015, Month.OCTOBER, 6) },
        { LocalDate.of(2015, Month.OCTOBER, 7) },
        { LocalDate.of(2015, Month.OCTOBER, 8) },
        { LocalDate.of(2015, Month.OCTOBER, 9) },
    });
  }


}