package io.ski.card.validator;

import io.ski.card.AbstractMockitoTest;
import io.ski.card.Card;
import io.ski.support.validation.BindingResult;
import io.ski.support.validation.HolidayResolver;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.time.*;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class WorkdayValidatorTest extends AbstractMockitoTest {

  private static final LocalDate SATURDAY_DATE = LocalDate.of(2015, Month.OCTOBER, 3);
  private static final LocalDate SUNDAY_DATE = LocalDate.of(2015, Month.OCTOBER, 4);
  private static final LocalDate HOLIDAY_DATE = LocalDate.of(2015, Month.DECEMBER, 25);

  private static final ZoneId DEFAULT_ZONE_ID = ZoneId.of("UTC");
  private static final ZoneOffset ZERO_ZONE_OFFSET = ZoneOffset.UTC;

  @Mock protected Clock clock;
  @Mock protected Card card;
  @Mock protected HolidayResolver holidayResolver;

  protected BindingResult bindingResult;
  protected WorkdayValidator<Card> validator;

  @Before
  public void setup() {
    bindingResult = new BindingResult();
    validator = new WorkdayValidator<>(clock);
    validator.setHolidayResolver(holidayResolver);

    when(clock.getZone()).thenReturn(DEFAULT_ZONE_ID);
  }

  @Test
  public void shouldConsiderSaturdayAsNotWorkday() {
    Instant saturdayInstant = dateToInstant(SATURDAY_DATE);
    when(clock.instant()).thenReturn(saturdayInstant);

    validator.validate(card, bindingResult);

    assertTrue(bindingResult.hasErrors());
  }

  @Test
  public void shouldConsiderSundayAsNotWorkday() {
    Instant sundayInstant = dateToInstant(SUNDAY_DATE);
    when(clock.instant()).thenReturn(sundayInstant);

    validator.validate(card, bindingResult);

    assertTrue(bindingResult.hasErrors());
  }

  @Test
  public void shouldConsiderHolidayAsNotWorkday() {
    Instant saturdayInstant = dateToInstant(HOLIDAY_DATE);
    when(clock.instant()).thenReturn(saturdayInstant);
    when(holidayResolver.isHoliday(HOLIDAY_DATE)).thenReturn(true);

    validator.validate(card, bindingResult);

    assertTrue(bindingResult.hasErrors());
  }

  protected static Instant dateToInstant(LocalDate date) {
    return date.atStartOfDay().toInstant(ZERO_ZONE_OFFSET);
  }
}