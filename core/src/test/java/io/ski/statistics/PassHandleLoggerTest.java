package io.ski.statistics;

import io.ski.card.AbstractMockitoTest;
import io.ski.card.Card;
import io.ski.statistics.domain.PassEvent;
import io.ski.statistics.repository.PassEventRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

public class PassHandleLoggerTest extends AbstractMockitoTest {
  @Mock private PassEventRepository passEventRepository;
  @Mock private Card card;

  private PassHandleLogger logger;

  @Before
  public void setup() {
    logger = new PassHandleLogger(passEventRepository);
  }

  @Test
  public void shouldPersistEventWhenCalled() {
    logger.postPassHandle(card);

    verify(passEventRepository).persist(any(PassEvent.class));
  }

}