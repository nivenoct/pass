package io.ski.repository.generator;

import io.ski.card.AbstractMockitoTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AutoIncrementationIdentifierGeneratorTest extends AbstractMockitoTest {

  private static final long CONSTRUCTOR_COUNTER_VALUE = 1L;
  private static final long INCREMENTED_COUNTER_VALUE = CONSTRUCTOR_COUNTER_VALUE + 1;

  private AutoIncrementationIdentifierGenerator generator;

  @Before
  public void setup() {
    generator = new AutoIncrementationIdentifierGenerator(CONSTRUCTOR_COUNTER_VALUE);
  }

  @Test
  public void shouldIncrementCounterWhenCalledGenerate() {
    generator.generate();
    assertEquals(generator.getCounter().longValue(), INCREMENTED_COUNTER_VALUE);
  }

}