package io.ski;

import io.ski.card.event.PostPassHandleListener;
import io.ski.card.event.PostValidationRejectionListener;
import io.ski.support.validation.BindingResult;
import org.junit.Test;
import org.mockito.stubbing.Answer;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class CardSystemPassTest extends AbstractCardSystemTest {

  @Test
  public void shouldApplyValidatorWhenCalledPass() {
    cardSystem.pass(defaultUserCard);

    verify(validator).validate(eq(defaultCard), any(BindingResult.class));
  }

  @Test
  public void shouldApplyHandlerWhenCalledPass() {
    cardSystem.pass(defaultUserCard);

    verify(passHandler).handle(eq(defaultCard));
  }

  @Test
  public void shouldApplyPostValidationRejectionListenerWhenCalledPassWithInvalidCard() {
    Answer<Void> answer = invocation -> {
      Object secondArgument = invocation.getArguments()[1];
      BindingResult bindingResult = (BindingResult) secondArgument;
      bindingResult.reject("some error");
      return null;
    };
    PostValidationRejectionListener postValidationRejectionListener = mock(PostValidationRejectionListener.class);

    doAnswer(answer).when(validator).validate(eq(defaultCard), any(BindingResult.class));

    cardSystem.addPostValidationRejectionListener(postValidationRejectionListener);
    cardSystem.pass(defaultUserCard);

    verify(postValidationRejectionListener).onPassRejection(eq(defaultCard), any(BindingResult.class));
  }

  @Test
  public void shouldApplyPostHandleListenerWhenCalledPass() {
    PostPassHandleListener postPassHandleListener = mock(PostPassHandleListener.class);

    cardSystem.addPostPassHandleListener(postPassHandleListener);
    cardSystem.pass(defaultUserCard);

    verify(postPassHandleListener).postPassHandle(eq(defaultCard));
  }
}