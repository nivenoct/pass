package io.ski;

import io.ski.card.AbstractMockitoTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

public class TurnstileTest extends AbstractMockitoTest {
  @Mock private CardSystem cardSystem;
  @Mock private UserCard dummyUserCard;

  private Turnstile turnstile;

  @Before
  public void setup() {
    turnstile = new Turnstile(cardSystem);
  }

  @Test
  public void shouldDelegatePassToSystem() {
    turnstile.pass(dummyUserCard);

    verify(cardSystem).pass(eq(dummyUserCard));
  }

}