package io.ski.statistics;

import io.ski.card.Card;
import io.ski.card.event.PostPassHandleListener;
import io.ski.statistics.domain.AuthorizedPassEvent;
import io.ski.statistics.repository.PassEventRepository;

import java.time.LocalDateTime;

public class PassHandleLogger implements PostPassHandleListener {

  private final PassEventRepository passEventRepository;

  public PassHandleLogger(PassEventRepository passEventRepository) {
    this.passEventRepository = passEventRepository;
  }

  @Override
  public void postPassHandle(Card card) {
    LocalDateTime now = LocalDateTime.now();
    AuthorizedPassEvent event = new AuthorizedPassEvent(now, card);
    passEventRepository.persist(event);
  }
}
