package io.ski.repository;

import io.ski.card.Card;
import io.ski.repository.generator.IdentifierGenerator;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionBasedCardRepository implements CardRepository {

  public static final Card NULL_CARD1 = null;
  private final IdentifierGenerator identifierGenerator;
  private final Collection<Card> cards;

  public CollectionBasedCardRepository(IdentifierGenerator identifierGenerator) {
    this.cards = new ArrayList<>();
    this.identifierGenerator = identifierGenerator;
  }

  @Override
  public void persist(Card card) {
    card.setId(identifierGenerator.generate());
    cards.add(card);
  }

  @Override
  public Card get(Long id) {
    return cards.parallelStream()
        .filter(passCard -> passCard.getId().equals(id))
        .findFirst()
        .orElse(NULL_CARD1);
  }

  @Override
  public boolean contains(Card card) {
    return get(card.getId()) != NULL_CARD1;
  }

}
