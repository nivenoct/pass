package io.ski;

import io.ski.card.Card;
import io.ski.card.CardDefinition;
import io.ski.card.Validator;
import io.ski.card.event.PostPassHandleListener;
import io.ski.card.event.PostValidationRejectionListener;
import io.ski.card.validator.support.HolidayResolverAware;
import io.ski.exception.AlreadyRegisteredCardTypeException;
import io.ski.exception.UnregisteredCardTypeException;
import io.ski.repository.CardRepository;
import io.ski.statistics.PassHandleLogger;
import io.ski.statistics.ValidationRejectionLogger;
import io.ski.statistics.View;
import io.ski.statistics.repository.CollectionBasedPassEventRepository;
import io.ski.statistics.repository.PassEventRepository;
import io.ski.support.validation.BindingResult;
import io.ski.support.validation.HolidayResolver;

import java.util.*;

public class CardSystem {
  private final Map<String, CardProvider<? extends Card>> cardProviderResolver;
  private final CardRepository cardRepository;
  private final PassEventRepository passEventRepository;
  private final HolidayResolver holidayResolver;

  private final Collection<PostPassHandleListener> postPassHandleListeners;
  private final Collection<PostValidationRejectionListener> postValidationRejectionListeners;

  public CardSystem(CardRepository cardRepository, HolidayResolver holidayResolver) {
    this.holidayResolver = holidayResolver;
    this.cardRepository = cardRepository;
    this.cardProviderResolver = new HashMap<>();
    this.postPassHandleListeners = new ArrayList<>();
    this.postValidationRejectionListeners = new ArrayList<>();

    this.passEventRepository = new CollectionBasedPassEventRepository();

    initEventLoggers();
  }

  public View createEventView() {
    return new View(this.passEventRepository);
  }

  public void registerCardType(CardDefinition<?> cardDefinition) {
    Objects.requireNonNull(cardDefinition.getDiscriminator());
    Objects.requireNonNull(cardDefinition.getCardFactory());
    Objects.requireNonNull(cardDefinition.getValidator());
    Objects.requireNonNull(cardDefinition.getPassHandler());

    String type = cardDefinition.getDiscriminator();
    if (cardProviderResolver.containsKey(type)) {
      throw new AlreadyRegisteredCardTypeException(type);
    }

    CardProvider<? extends Card> cardProvider = toProvider(cardDefinition);
    cardProviderResolver.put(type, cardProvider);

    postRegistration(cardProvider);
  }

  public UserCard create(String cardDiscriminator) {
    Card card = createInstance(cardDiscriminator);
    cardRepository.persist(card);
    return UserCard.of(card);
  }

  public boolean pass(UserCard userCard) {
    Card card = getCard(userCard);
    BindingResult bindingResult = new BindingResult();
    applyValidator(card, bindingResult);
    if (bindingResult.hasErrors()) {
      applyPostValidationRejectionListeners(card, bindingResult);
      return false;
    }
    applyHandler(card);
    applyPostHandleListeners(card);
    return true;
  }

  public void block(UserCard userCard) {
    setBlock(userCard, true);
  }

  public void unblock(UserCard userCard) {
    setBlock(userCard, false);
  }

  public void addPostPassHandleListener(PostPassHandleListener listener) {
    postPassHandleListeners.add(listener);
  }

  public void addPostValidationRejectionListener(PostValidationRejectionListener listener) {
    postValidationRejectionListeners.add(listener);
  }

  private void setBlock(UserCard userCard, boolean block) {
    Card card = getCard(userCard);
    cardRepository.get(card.getId()).setBlocked(block);
  }

  private Card createInstance(String cardType) {
    return getProvider(cardType).getCardFactory().create();
  }

  private void postRegistration(CardProvider<? extends Card> provider) {
    Validator<? extends Card> validator = provider.getValidator();
    if (validator instanceof HolidayResolverAware) {
      ((HolidayResolverAware) validator).setHolidayResolver(holidayResolver);
    }
  }

  @SuppressWarnings("unchecked")
  private void applyValidator(Card card, BindingResult bindingResult) {
    String cardDiscriminator = card.getDiscriminator();
    getProvider(cardDiscriminator).getValidator().validate(card, bindingResult);
  }

  @SuppressWarnings("unchecked")
  private void applyHandler(Card card) {
    String cardDiscriminator = card.getDiscriminator();
    getProvider(cardDiscriminator).getPassHandler().handle(card);
  }

  private void applyPostValidationRejectionListeners(Card card, BindingResult bindingResult) {
    postValidationRejectionListeners.parallelStream().forEach(l -> l.onPassRejection(card, bindingResult));
  }

  private void applyPostHandleListeners(Card card) {
    postPassHandleListeners.parallelStream().forEach(l -> l.postPassHandle(card));
  }

  private CardProvider getProvider(String cardType) {
    return Optional.ofNullable(cardProviderResolver.get(cardType)).orElseThrow(() -> new UnregisteredCardTypeException(cardType));
  }

  private Card getCard(UserCard userCard) {
    return cardRepository.get(userCard.getId());
  }

  private void initEventLoggers() {
    addPostValidationRejectionListener(new ValidationRejectionLogger(this.passEventRepository));
    addPostPassHandleListener(new PassHandleLogger(this.passEventRepository));
  }

  @SuppressWarnings("unchecked")
  private static CardProvider<? extends Card> toProvider(CardDefinition<? extends Card> cardDefinition) {
    return new CardProvider(
        cardDefinition.getCardFactory(),
        cardDefinition.getValidator(),
        cardDefinition.getPassHandler()
    );
  }
}
