package io.ski.card.validator;

import io.ski.support.validation.HolidayResolver;

import java.time.DayOfWeek;
import java.time.LocalDate;

public final class ValidatorUtils {

  private ValidatorUtils() {
  }

  public static boolean isNotWorkingDay(HolidayResolver holidayResolver, LocalDate localDate) {
    DayOfWeek dayOfWeek = localDate.getDayOfWeek();
    return dayOfWeek.equals(DayOfWeek.SATURDAY)
        || dayOfWeek.equals(DayOfWeek.SUNDAY)
        || holidayResolver.isHoliday(localDate);
  }

  public static boolean isWorkingDay(HolidayResolver holidayResolver, LocalDate localDate) {
    return !isNotWorkingDay(holidayResolver, localDate);
  }
}
