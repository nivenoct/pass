package io.ski.card.handler;

import io.ski.card.PassHandler;
import io.ski.card.type.UnlimitedCard;

public class UnlimitedHandler<T extends UnlimitedCard> implements PassHandler<T> {

  @Override
  public void handle(T card) { }
}
