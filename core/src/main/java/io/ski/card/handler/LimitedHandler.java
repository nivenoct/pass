package io.ski.card.handler;

import io.ski.card.PassHandler;
import io.ski.card.type.LimitedCard;

public class LimitedHandler<T extends LimitedCard> implements PassHandler<T> {

  @Override
  public void handle(T card) {
    card.setCounter(card.getCounter() - 1);
  }
}
