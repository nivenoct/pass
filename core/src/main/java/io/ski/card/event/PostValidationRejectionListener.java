package io.ski.card.event;

import io.ski.card.Card;
import io.ski.support.validation.BindingResult;

public interface PostValidationRejectionListener {
  void onPassRejection(Card card, BindingResult bindingResult);
}
