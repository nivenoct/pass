package io.ski.card.event;

import io.ski.card.Card;

public interface PostPassHandleListener {
  void postPassHandle(Card card);
}
