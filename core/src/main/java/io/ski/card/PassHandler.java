package io.ski.card;

public interface PassHandler<T extends Card> {
  void handle(T card);
}
