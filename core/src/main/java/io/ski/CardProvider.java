package io.ski;

import io.ski.card.PassHandler;
import io.ski.card.Card;
import io.ski.card.CardFactory;
import io.ski.card.Validator;

public class CardProvider<T extends Card> {
  private final CardFactory<T> cardFactory;
  private final Validator<T> validator;
  private final PassHandler<T> passHandler;

  public CardProvider(CardFactory<T> cardFactory, Validator<T> validator, PassHandler<T> passHandler) {
    this.cardFactory = cardFactory;
    this.validator = validator;
    this.passHandler = passHandler;
  }

  public CardFactory<T> getCardFactory() {
    return cardFactory;
  }

  public Validator<T> getValidator() {
    return validator;
  }

  public PassHandler<T> getPassHandler() {
    return passHandler;
  }
}