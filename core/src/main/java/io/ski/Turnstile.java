package io.ski;

public class Turnstile {
  private final CardSystem cardSystem;

  public Turnstile(CardSystem cardSystem) {
    this.cardSystem = cardSystem;
  }

  public boolean pass(UserCard userCard) {
    return cardSystem.pass(userCard);
  }
}
